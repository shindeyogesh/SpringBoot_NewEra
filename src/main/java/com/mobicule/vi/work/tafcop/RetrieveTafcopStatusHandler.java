package com.mobicule.vi.work.tafcop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mobicule.vi.NewEra.entities.Request;
import com.mobicule.vi.NewEra.entities.Response;
import com.mobicule.vi.NewEra.exceptions.ValueFromPropertyFileNotFoundException;
import com.mobicule.vi.NewEra.filters.services.impl.ITransactionServiceImpl;

public class RetrieveTafcopStatusHandler extends ITransactionServiceImpl {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public Response processRequest(Request request) throws Exception {
		log.info("processing request...");
		List<Map<String, String>> dataList = new ArrayList<>();
		Map<String, String> dataMap = new HashMap<>();
		dataMap.put("Name1", "Password1");
		dataMap.put("Name2", "Password2");
		dataList.add(dataMap);

		if (dataMap == null || dataMap.isEmpty())
			throw new ValueFromPropertyFileNotFoundException("prop", "key");
		return new Response("Everything is good", "SUCCESS", dataList);
	}
}
