package com.mobicule.vi.work.nonEkyc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.mobicule.vi.NewEra.entities.Request;
import com.mobicule.vi.NewEra.entities.Response;
import com.mobicule.vi.NewEra.filters.services.impl.ITransactionServiceImpl;

public class CheckAddressStatusHandler extends ITransactionServiceImpl {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Environment env;

	@Override
	public Response processRequest(Request request) {
		log.info("processing request...");
		List<Map<String, String>> dataList = new ArrayList<>();
		Map<String, String> dataMap = new HashMap<>();
		dataMap.put("Key1", "Value1");
		dataMap.put("Key2", "Value2");
		dataList.add(dataMap);

		String[] activeProfiles = env.getActiveProfiles();
		log.info("Active Profile : " + activeProfiles[0]);

		return new Response("All is well", "SUCCESS", dataList);
	}
}
