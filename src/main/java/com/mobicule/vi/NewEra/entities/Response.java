package com.mobicule.vi.NewEra.entities;

import java.util.List;

public class Response {

	private String message;
	private String status;
	private List data = null;

	@Override
	public String toString() {
		return "Response [message=" + message + ", status=" + status + ", data=" + data + "]";
	}

	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Response(String message, String status, List data) {
		super();
		this.message = message;
		this.status = status;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

}