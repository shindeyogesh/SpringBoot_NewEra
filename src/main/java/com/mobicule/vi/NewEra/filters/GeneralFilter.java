package com.mobicule.vi.NewEra.filters;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.crypto.SecretKey;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobicule.component.commons.utilities.PlatformUtility;
import com.mobicule.component.security.AESEncryption;
import com.mobicule.component.sync.UnauthorizedAccessException;
import com.mobicule.vi.NewEra.entities.Response;
import com.mobicule.vi.NewEra.entities.UserDetails;
import com.mobicule.vi.NewEra.exceptions.PropertyFileNotFoundException;
import com.mobicule.vi.NewEra.exceptions.PropertyFilePathNotFoundException;
import com.mobicule.vi.NewEra.exceptions.ValueFromPropertyFileNotFoundException;
import com.mobicule.vi.NewEra.filters.services.CachedBodyHttpServletRequest;
import com.mobicule.vi.NewEra.filters.services.impl.CommonMethods;

@Component
public class GeneralFilter implements Filter {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ObjectMapper mapper;

	private CachedBodyHttpServletRequest cachedBodyHttpServletRequest;

	ClassLoader classloader = Thread.currentThread().getContextClassLoader();

	@Autowired
	private CommonMethods utility;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		log.info("In doFilter method of GeneralFilter");

		// Casting ServletRequest to HttpServletRequest
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		String methodType = httpRequest.getMethod();
		log.info("Method type  " + methodType);
		if (methodType.equalsIgnoreCase("GET")) {
			chain.doFilter(request, response);
			return;
		}

		// Getting Headers
		Map<String, String> headersMap = getHeadersInfo(httpRequest);
		log.info("Headers : " + mapper.writeValueAsString(headersMap));

		String isDataCompressed = headersMap.get("compressed") != null ? headersMap.get("compressed") : "false";
		log.info("Is data compressed : " + isDataCompressed);

		// Reading Request Data
		String data = isDataCompressed.equalsIgnoreCase("true") ? this.readZippedBody(httpRequest)
				: this.readBody(httpRequest);
		log.info("Data : " + data);

		// Creating Response In Case Of Errors
		Response errorDetails;
		httpResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);

		try {
			// Configuring Application
			Map<String, String> appSetting = utility.getAppSettings();
			log.info("App setting : " + appSetting.toString());

			// Getting Request Digest Property
			String requestDigest = appSetting.get("request_digest");
			log.info("Request digest : " + requestDigest);

			if (requestDigest.equals("true")) {
				String digest = headersMap.get("digest");
				String salt = headersMap.get("salt");
				if (salt != null) {
					log.info("Salt is not null : " + salt);
					AESEncryption aes = new AESEncryption();
					SecretKey secretKey = aes.decryptSecretKey(salt, appSetting);
					log.info("Secret key : " + secretKey);
					String saltDecrypt = new String(Base64.encodeBase64(secretKey.getEncoded()));
					requestDigest = PlatformUtility.generateDigest(data, saltDecrypt);
				} else {
					log.info("In seed : " + appSetting.get("seed"));
					requestDigest = PlatformUtility.generateDigest(data, appSetting.get("seed"));
				}
				if (!PlatformUtility.checkDigest(digest, requestDigest)) {
					log.info("Request digest mismatch");
					throw new UnauthorizedAccessException("Request digest mismatch");
				}
			}

			String sessionToken = headersMap.get("session-token");
			log.info("Session token : " + sessionToken);

			try {
				JSONObject userJson = getUSerJsonFromHttpRequest(data);
				log.info("User json : " + userJson);

				String etop = userJson.getString("etop");
				log.info("Etop : " + etop);

				String sessionTokenOnOffFlag = utility.getPropertyFilePath("SessionTokenOnOffFlag");

				String onOffFlag = utility.getValueFromPropertyFile(sessionTokenOnOffFlag, "SessionTokenOnOffFlag");
				log.info("session token on/off flag : " + onOffFlag);

				String tokenValidTimeFlag = utility.getValueFromPropertyFile(sessionTokenOnOffFlag,
						"TokenValidTimeFlag");
				int tokenValidTime = tokenValidTimeFlag != null ? Integer.parseInt(tokenValidTimeFlag) : 0;
				log.info("token valid time flag : " + tokenValidTime);

				String mandatoryFlag = utility.getValueFromPropertyFile(sessionTokenOnOffFlag,
						"SessionTokenMandatoryFlag");
				httpResponse.setHeader("compressed", isDataCompressed);

				if (onOffFlag.equalsIgnoreCase("true")) {
					if (headersMap.containsKey("session-token")) {
						Map<String, String> queryMap = new HashMap<>();
						queryMap.put("ETOP_NUMBER", etop);

						List<UserDetails> resultList = new ArrayList<>();
						UserDetails userDetails = new UserDetails();
						userDetails.setSession_token("Hi");
						userDetails.setToken_createdOn(Timestamp.valueOf("2022-10-17 16:00:15"));
						resultList.add(userDetails);
						log.info("Inside validate session result : " + resultList);

						if (resultList != null && !resultList.isEmpty()) {
							userDetails = resultList.get(0);
							if (userDetails != null && userDetails.getSession_token() != null
									&& userDetails.getSession_token().equalsIgnoreCase(sessionToken)) {
								if (calculateTokenGeneratedTimeDiff(
										userDetails.getToken_createdOn()) < tokenValidTime) {
									log.info("Valid user with valid token");
									httpResponse.setHeader("session-token", sessionToken);
									chain.doFilter(cachedBodyHttpServletRequest, httpResponse);
									return;
								} else {
									String sessionTokenNumber = generateSessionTokenId(etop);
									userDetails.setSession_token(sessionTokenNumber);
									userDetails.setToken_createdOn(new Timestamp(System.currentTimeMillis()));
									// dbTransactionService.update(userDetails);
									log.warn("Valid user with expired token");
									httpResponse.setHeader("session-token", sessionTokenNumber);
									chain.doFilter(cachedBodyHttpServletRequest, httpResponse);
									return;
								}
							} else {
								httpResponse.setHeader("session-token", sessionToken);
								httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
								log.error("No valid token found");
								errorDetails = new Response("No valid token found", "FAILURE", new ArrayList<>());
								mapper.writeValue(httpResponse.getWriter(), errorDetails);
								return;
							}
						} else {
							httpResponse.setHeader("session-token", sessionToken);
							httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
							log.error("User not found in the DB");
							errorDetails = new Response("User not found with the given etop number", "FAILURE",
									new ArrayList<>());
							mapper.writeValue(httpResponse.getWriter(), errorDetails);
							return;
						}
					} else if (mandatoryFlag.equalsIgnoreCase("true")) {
						httpResponse.setHeader("session-token", sessionToken);
						httpResponse.setStatus(HttpStatus.FORBIDDEN.value());
						log.error("Mandatory flag true need session token to proceed");
						errorDetails = new Response("Invalid session, kindly re-login to start new session", "FAILURE",
								new ArrayList<>());
						mapper.writeValue(httpResponse.getWriter(), errorDetails);
						return;
					} else {
						httpResponse.setHeader("session-token", sessionToken);
						log.warn("Session token mandatory off");
						chain.doFilter(cachedBodyHttpServletRequest, httpResponse);
						return;
					}
				} else {
					httpResponse.setHeader("session-token", sessionToken);
					log.warn("Session token demand in off mode");
					chain.doFilter(cachedBodyHttpServletRequest, httpResponse);
					return;
				}
			} catch (PropertyFilePathNotFoundException e) {
				log.error("****Property File Path Not Found******");
				httpResponse.setHeader("session-token", sessionToken);
				httpResponse.setStatus(HttpStatus.NOT_FOUND.value());
				errorDetails = new Response(e.getMessage(), "FAILURE", new ArrayList<>());
				mapper.writeValue(httpResponse.getWriter(), errorDetails);
				return;
			} catch (PropertyFileNotFoundException e) {
				log.error("****Property File Not Found******");
				httpResponse.setHeader("session-token", sessionToken);
				httpResponse.setStatus(HttpStatus.NOT_FOUND.value());
				errorDetails = new Response(e.getMessage(), "FAILURE", new ArrayList<>());
				mapper.writeValue(httpResponse.getWriter(), errorDetails);
				return;
			} catch (ValueFromPropertyFileNotFoundException e) {
				log.error("****Value From Property File Is Not Found******");
				httpResponse.setHeader("session-token", sessionToken);
				httpResponse.setStatus(HttpStatus.NOT_FOUND.value());
				errorDetails = new Response(e.getMessage(), "FAILURE", new ArrayList<>());
				mapper.writeValue(httpResponse.getWriter(), errorDetails);
				return;
			} catch (Exception e) {
				e.printStackTrace();
				httpResponse.setHeader("session-token", sessionToken);
				httpResponse.setStatus(HttpStatus.BAD_REQUEST.value());
				log.error("****Exception Occured Sesion Filter******");
				errorDetails = new Response("Invalid session, kindly re-login to start new session", "FAILURE",
						new ArrayList<>());
				mapper.writeValue(httpResponse.getWriter(), errorDetails);
				return;
			}
		} catch (UnauthorizedAccessException e) {
			log.error("UnauthorizedAccessException : ", e);
			errorDetails = new Response(e.getMessage(), "FAILURE", new ArrayList<>());
			httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
			mapper.writeValue(httpResponse.getWriter(), errorDetails);
			return;
		} catch (Exception e) {
			log.error("exception : ", e);
			errorDetails = new Response(e.getMessage(), "FAILURE", new ArrayList<>());
			httpResponse.setStatus(HttpStatus.BAD_REQUEST.value());
			mapper.writeValue(httpResponse.getWriter(), errorDetails);
			return;
		}
	}

	private String readBody(HttpServletRequest request) throws IOException {
		this.cachedBodyHttpServletRequest = new CachedBodyHttpServletRequest(request);
		InputStream requestInputStream = cachedBodyHttpServletRequest.getInputStream();
		byte[] cachedBody = StreamUtils.copyToByteArray(requestInputStream);
		return new String(cachedBody, "UTF-8");
	}

	private String readZippedBody(HttpServletRequest request) throws IOException {
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		GZIPInputStream gis = new GZIPInputStream(request.getInputStream());
		byte[] data = new byte[8192];
		int nRead;
		while ((nRead = gis.read(data, 0, data.length)) != -1) {
			arrayOutputStream.write(data, 0, nRead);
		}
		return new String(arrayOutputStream.toByteArray(), "UTF-8");
	}

	private Map<String, String> getHeadersInfo(HttpServletRequest request) {
		Map<String, String> headerMap = new HashMap<String, String>();
		@SuppressWarnings("rawtypes")
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);
			headerMap.put(key, value);
		}
		return headerMap;
	}

	private JSONObject getUSerJsonFromHttpRequest(String jsonString) {
		JSONObject userJson = null;
		try {
			log.info("inside user json retrival");
			JSONObject json = new JSONObject(jsonString);
			String userJsonstr = json.optString("user");
			userJson = new JSONObject(userJsonstr);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return userJson;
	}

	private long calculateTokenGeneratedTimeDiff(Timestamp tokenCreatedOnDateTime) {
		Timestamp currentDateTime = new Timestamp(System.currentTimeMillis());
		Date tokenSentOnDate = new Date(tokenCreatedOnDateTime.getTime());
		Date currentDateDate = new Date(currentDateTime.getTime());
		long timeDiffMilliSeconds = (currentDateDate.getTime() - tokenSentOnDate.getTime());
		long timediffMinutes = (timeDiffMilliSeconds / 1000) / 60;
		log.info("TOKEN SENT TIME DIFF IN MIN " + timediffMinutes);
		return timediffMinutes;
	}

	public String generateSessionTokenId(String etop) {
		int randomNumber = 0;
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder alphabets = new StringBuilder();
		randomNumber = ((int) ((Math.random() * 90000) + 10000));
		for (int i = 0; i < 2; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());
			alphabets.append(AlphaNumericString.charAt(index));
		}
		return "" + alphabets + randomNumber + System.currentTimeMillis() + etop.substring(5);
	}
}