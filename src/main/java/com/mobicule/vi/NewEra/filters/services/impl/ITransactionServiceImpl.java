package com.mobicule.vi.NewEra.filters.services.impl;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mobicule.vi.NewEra.entities.Request;
import com.mobicule.vi.NewEra.entities.Response;
import com.mobicule.vi.NewEra.filters.services.ITransactionService;

public class ITransactionServiceImpl implements ITransactionService {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public Response processRequest(Request request) throws Exception {
		log.info("processing request...");
		return new Response("Success", "SUCCESS", new ArrayList<>());
	}
}