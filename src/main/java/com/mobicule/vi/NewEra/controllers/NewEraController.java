package com.mobicule.vi.NewEra.controllers;

import java.io.InputStream;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;

import com.mobicule.vi.NewEra.entities.Request;
import com.mobicule.vi.NewEra.entities.Response;
import com.mobicule.vi.NewEra.exceptions.HandlerBeanNotFoundException;
import com.mobicule.vi.NewEra.exceptions.HandlerNotFoundException;
import com.mobicule.vi.NewEra.filters.services.ITransactionService;

@RestController
public class NewEraController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private Object clsInstance = null;

	private ITransactionService genericTransactionService;
	
	@Autowired
    private  AutowireCapableBeanFactory autowireCapableBeanFactory;

	@Autowired
	private Environment env;

	@GetMapping("/NewEra")
	public String getNewEra() {
		log.info("Inside getNewEra Method of NewEraController");
		return "New Era Up & Running<br>17/10/2022";
	}

	@PostMapping("/NewEra")
	public Response postNewEra(@RequestBody Request request) throws Exception {
		log.info("Inside postNewEra method of NewEraController : " + request);
		String entity = request.getEntity();
		log.info("entity : " + entity);
		String handlerBean = getHandler(entity);
		Class<?> currentEntity;
		try {
			currentEntity = Class.forName(handlerBean);
			clsInstance = currentEntity.newInstance();
			autowireCapableBeanFactory.autowireBean(clsInstance);
		} catch (ClassNotFoundException e) {
			throw new HandlerNotFoundException(handlerBean);
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.genericTransactionService = (ITransactionService) clsInstance;

		Response response = genericTransactionService.processRequest(request);

		log.info("Response : " + response);

		return response;
	}

	public String getHandler(String entity) throws HandlerBeanNotFoundException {
		InputStream inputStream;
		String handlerName = null;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		inputStream = classloader.getResourceAsStream(env.getProperty("entityMapping"));
		Map<String, String> data = new Yaml().load(inputStream);
		log.info("All Entity Mapping : " + data);
		handlerName = data.get(entity);
		log.info("Handler Name : " + handlerName);
		if (handlerName == null)
			throw new HandlerBeanNotFoundException(entity);
		return handlerName;
	}
}
