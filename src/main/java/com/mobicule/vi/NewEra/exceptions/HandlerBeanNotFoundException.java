package com.mobicule.vi.NewEra.exceptions;

import java.io.FileNotFoundException;

public class HandlerBeanNotFoundException extends FileNotFoundException {
	private static final long serialVersionUID = 1L;

	public HandlerBeanNotFoundException(String entityName) {
		super(String.format("Handler bean against entity : '%s' is not found in the Entity Mapping File", entityName));
	}
}
