package com.mobicule.vi.NewEra.exceptions;

public class HandlerNotFoundException extends ClassNotFoundException {

	private static final long serialVersionUID = 1L;

	public HandlerNotFoundException(String handlerBean) {
		super(String.format("Handler against bean : '%s' is not found", handlerBean));
	}
}
