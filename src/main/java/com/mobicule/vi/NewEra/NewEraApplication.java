package com.mobicule.vi.NewEra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class NewEraApplication extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(NewEraApplication.class, args);
	}
}
